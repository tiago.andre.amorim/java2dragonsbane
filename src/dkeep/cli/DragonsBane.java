package dkeep.cli;

import java.util.Scanner;

import dkeep.logic.*;

public class DragonsBane {
  public static void main(String[] args) throws Exception {

    // Game loop
    Scanner inputScanner = new Scanner(System.in);
    char userInput;
    boolean isGameOver = false;

    int ndragons = askNumberOfDragons(inputScanner);

    Game game = new Game(ndragons);

    do {
      // print the maze
      printMaze(game.getMaze());

      // read user command
      System.out.print("cmd> ");
      userInput = inputScanner.next().charAt(0); // ignore the other characters

      // update the game
      isGameOver = game.updateGame(userInput);
      if (!isGameOver)
        System.out.println(game.getOutputMessage());

    } while ((!isGameOver) && (userInput != 'q'));

    // final state of the game
    printMaze(game.getMaze());
    System.out.println(game.getOutputMessage());

    System.out.println("Exiting!");

    inputScanner.close();
  }

  private static int askNumberOfDragons(Scanner s) {
    do {
      System.out.print("How many dragons in the maze? (1-4) ");

      String n = s.next();

      try {
        int nd = Integer.parseInt(n);
        if ((nd <= 0) || (nd > 4))
          throw new NumberFormatException();

        return nd;
      } catch (NumberFormatException e) {
        System.out.println("Please enter a valid number");
      }
    } while (true);
  }

  private static void printMaze(char[][] maze) {
    for (int i = 0; i < maze.length; i++) {
      // - print the maze coordinate (i,j)
      for (int j = 0; j < maze[i].length; j++)
        System.out.print(maze[i][j] + " ");

      // - add linebreak after each line of the maze
      System.out.println();
    }
  }
}

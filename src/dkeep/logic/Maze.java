package dkeep.logic;

import java.util.ArrayList;

public class Maze {
  char[][] maze = { { 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' },
      { 'X', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
      { 'X', ' ', ' ', ' ', ' ', ' ', ' ', 'X', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
      { 'X', ' ', 'X', 'X', ' ', ' ', ' ', ' ', ' ', 'X' },
      { 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' } };

  enum direction {
    kNorth, kSouth, kEast, kWest
  };

  public char[][] getMaze() {
    return maze;
  }

  public int getNRows() {
    return maze.length;
  }

  public int getNCols() {
    return maze[0].length;
  }

  public void clearCell(int x, int y) {
    maze[x][y] = ' ';
  }

  public boolean isExit(int x, int y) {
    return (maze[x][y] == 'E');
  }

  public boolean isWall(int x, int y) {
    return (maze[x][y] == 'X');
  }

  public boolean isInteriorWall(int x, int y) {
    for (direction dir : direction.values())
      if (isThere(' ', x, y, dir))
        return false;

    return true;
  }

  public boolean isThere(char ch, int here_x, int here_y, direction dir) {

    if ((dir == direction.kNorth) && (here_x - 1 >= 0) && (maze[here_x - 1][here_y] == ch))
      return true;
    if ((dir == direction.kSouth) && (here_x + 1 <= maze.length - 1) && (maze[here_x + 1][here_y] == ch))
      return true;
    if ((dir == direction.kEast) && (here_y + 1 <= maze[0].length - 1) && (maze[here_x][here_y + 1] == ch))
      return true;
    if ((dir == direction.kWest) && (here_y - 1 >= 0) && (maze[here_x][here_y - 1] == ch))
      return true;

    return false;
  }

  public boolean canMoveInto(int x, int y) {
    return ((maze[x][y] == ' ') || (maze[x][y] == 'S'));
  }

  public void moveElement(char ch, int sx, int sy, int dx, int dy) {
    clearCell(sx, sy);
    maze[dx][dy] = ch;
  }

  public void placeElement(char ch, int x, int y) {
    maze[x][y] = ch;
  }

  public ArrayList<int[]> getFreeCells() {
    ArrayList<int[]> freeCells = new ArrayList<int[]>();

    int n = 0;
    for (int i = 0; i < 10; i++)
      for (int j = 0; j < 10; j++)
        if (canMoveInto(i, j)) {
          freeCells.add(n, new int[2]);
          freeCells.get(n)[0] = i;
          freeCells.get(n)[1] = j;
          n++;
        }
    return freeCells;
  }
}
package dkeep.logic;

import dkeep.logic.Maze.direction;

public class Dragon extends MoveableElement {
  boolean alive;
  boolean over_sword;

  public Dragon(Maze maze, int x, int y) {
    super(maze, x, y);
    alive = true;
    over_sword = false;
  }

  public boolean isAlive() {
    return alive;
  }

  public void setAlive(boolean alive) {
    this.alive = alive;
  }

  public void move() {
    int here_x = x;
    int here_y = y;
    int moveAmount = (int) (Math.random() * 3);

    direction dir = direction.values()[(int) (Math.random() * 4)];

    for (int i = 0; i < moveAmount; i++) {
      if (over_sword) {
        if (move('D', dir)) {
          maze.placeElement('S', here_x, here_y);
          over_sword = false;
        }
        return;

      } else if (maze.isThere('S', here_x, here_y, dir)) {
        move('F', dir);
        over_sword = true;
      }

      if (!move('D', dir))
        return;
    }
  }
}

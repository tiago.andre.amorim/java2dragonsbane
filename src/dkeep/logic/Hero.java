package dkeep.logic;

import dkeep.logic.Maze.direction;

public class Hero extends MoveableElement {
  boolean is_armed;
  boolean has_key;
  char ch;

  public Hero(Maze maze, int x0, int y0) {
    super(maze, x0, y0);
    is_armed = false;
    has_key = false;
    ch = 'H';
  }

  public boolean isArmed() {
    return is_armed;
  }

  public boolean hasKey() {
    return has_key;
  }

  public void setArmed(boolean is_armed) {
    this.is_armed = is_armed;
    if (is_armed)
      this.ch = 'A';
    maze.placeElement(ch, x, y);
  }

  public void setHasKey(boolean has_key) {
    this.has_key = has_key;
  }

  public void move(direction dir) {
    super.move(ch, dir);
  }
}

package dkeep.logic;

import java.util.ArrayList;

import dkeep.logic.Maze.direction;

public class Game {
  Maze maze;
  Hero hero;
  ArrayList<Dragon> dragons;
  Element sword;
  boolean has_key;
  String output_msg;

  final int MIN_SPAWN_DISTANCE_DIFF_ELEMENTS = 2;

  public Game(int ndragons) {
    maze = new Maze();

    createExit();

    ArrayList<int[]> freeCells = maze.getFreeCells();
    freeCells = createHero(freeCells);
    freeCells = createSword(freeCells);
    freeCells = createDragons(freeCells, ndragons);
  }

  public char[][] getMaze() {
    return maze.getMaze();
  }

  public String getOutputMessage() {
    return output_msg;
  }

  public boolean updateGame(char uc) {
    output_msg = "";

    switch (uc) {
      case 'w':
        return UpdateTurn(direction.kNorth);
      case 's':
        return UpdateTurn(direction.kSouth);
      case 'd':
        return UpdateTurn(direction.kEast);
      case 'a':
        return UpdateTurn(direction.kWest);
    }

    return false;
  }

  private boolean UpdateTurn(direction dir) {
    if (!TryExit(dir)) {
      hero.move(dir);
      TryToPickSword();
      if ((!AreAllDragonsDead()) && (!DragonsKilledHero())) {
        return DragonsMove();
      } else if (AreAllDragonsDead())
        return false;
      else
        return true;
    } else
      return true;
  }

  private boolean AreAllDragonsDead() {
    for (Dragon dragon : dragons)
      if (dragon.isAlive())
        return false;

    return true;
  }

  // transform a wall into an exit
  private void createExit() {
    int[] exitPos = new int[2];

    // select a random cell and check if it is a wall
    // if it is a wall, place the exit there
    // for small maps this will be fast enough
    for (;;) {
      exitPos[0] = (int) (Math.random() * maze.getNRows());
      exitPos[1] = (int) (Math.random() * maze.getNCols());

      // place the exit on a wall that is accessible to the hero
      if (maze.isWall(exitPos[0], exitPos[1]) && !maze.isInteriorWall(exitPos[0], exitPos[1]))
        break;
    }

    maze.placeElement('E', exitPos[0], exitPos[1]);

  }

  private ArrayList<int[]> createHero(ArrayList<int[]> freeCells) {
    int[] heroPos = freeCells.remove((int) (Math.random() * freeCells.size()));
    hero = new Hero(maze, heroPos[0], heroPos[1]);
    maze.placeElement('H', heroPos[0], heroPos[1]);

    // remove positions within minimum spawn distance to hero from freeCells
    freeCells.removeIf((int[] pos) -> {
      return (Math.abs(pos[0] - heroPos[0]) <= MIN_SPAWN_DISTANCE_DIFF_ELEMENTS)
          && (Math.abs(pos[1] - heroPos[1]) <= MIN_SPAWN_DISTANCE_DIFF_ELEMENTS);
    });

    return freeCells;
  }

  private ArrayList<int[]> createSword(ArrayList<int[]> freeCells) {
    int[] swordPos = freeCells.remove((int) (Math.random() * freeCells.size()));
    sword = new Element(maze, swordPos[0], swordPos[1]);
    maze.placeElement('S', swordPos[0], swordPos[1]);

    return freeCells;
  }

  private ArrayList<int[]> createDragons(ArrayList<int[]> freeCells, int ndragons) {
    // create dragons
    dragons = new ArrayList<Dragon>(ndragons);
    for (int i = 0; i < ndragons; i++) {
      int[] dragonPos = freeCells.remove((int) (Math.random() * freeCells.size()));
      dragons.add(i, new Dragon(maze, dragonPos[0], dragonPos[1]));
      maze.placeElement('D', dragonPos[0], dragonPos[1]);
    }

    return freeCells;
  }

  private boolean DragonsKilledHero() {
    for (Dragon dragon : dragons) {
      if ((dragon.isAlive()) && (dragon.adjacentTo(hero))) {
        if (hero.is_armed) {
          dragon.setAlive(false);
          maze.clearCell(dragon.getX(), dragon.getY());
          hero.setHasKey(AreAllDragonsDead());
        } else {
          output_msg = "YOU DIED!";
          return true;
        }
      }
    }
    return false;
  }

  private boolean DragonsMove() {
    for (Dragon dragon : dragons) {
      if (!dragon.isAlive())
        continue;
      dragon.move();
    }
    return DragonsKilledHero();
  }

  private void TryToPickSword() {
    if (hero.isArmed())
      return;
    hero.setArmed(hero.overlap(sword));
  }

  private boolean TryExit(direction dir) {
    int x = hero.getX();
    int y = hero.getY();

    boolean next_to_exit = maze.isThere('E', x, y, dir);

    if (next_to_exit) {
      if (hero.hasKey()) {
        output_msg = "GREAT! YOU REACHED THE EXIT!";
        return true;
      } else {
        output_msg = "You need a key to open this door.";
        return false;
      }
    }

    return false;
  }
}

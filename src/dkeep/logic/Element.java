package dkeep.logic;

public class Element {
  Maze maze;
  int x;
  int y;

  public Element(Maze maze, int x, int y) {
    this.maze = maze;
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }

  public boolean adjacentTo(Element other) {
    if ((x - 1 == other.x) && (y == other.y))
      return true; // north
    if ((x + 1 == other.x) && (y == other.y))
      return true; // south
    if ((x == other.x) && (y + 1 == other.y))
      return true; // east
    if ((x == other.x) && (y - 1 == other.y))
      return true; // west

    return false;
  }

  public boolean overlap(Element other) {
    return ((x == other.x) && (y == other.y));
  }
}

package dkeep.logic;

import dkeep.logic.Maze.direction;

public class MoveableElement extends Element {
  public MoveableElement(Maze maze, int x, int y) {
    super(maze, x, y);
  }

  public boolean move(char ch, direction dir) {
    int delta_x = 0;
    int delta_y = 0;

    // Check direction
    if (dir == direction.kNorth)
      delta_x = -1;
    if (dir == direction.kSouth)
      delta_x = 1;
    if (dir == direction.kEast)
      delta_y = 1;
    if (dir == direction.kWest)
      delta_y = -1;

    // Check if it is possible to move the hero in the maze
    if (maze.canMoveInto(x + delta_x, y + delta_y)) {
      // - update representation in the maze
      maze.moveElement(ch, x, y, x + delta_x, y + delta_y);

      // - update pose (x,y)
      x += delta_x;
      y += delta_y;

      return true;
    }

    return false;
  }
}
